# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [4.0.0 - 2018-01-08]
### Added
- Add option to add custom default headers

### Changed

### Deprecated

### Removed
- Removed redundant throws clauses

### Fixed

### Security